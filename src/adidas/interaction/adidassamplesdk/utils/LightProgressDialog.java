package adidas.interaction.adidassamplesdk.utils;



import adidas.interaction.adidassamplesdk.R;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;

import android.widget.TextView;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.FROYO;
import static android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH;


/**
 * Progress dialog in Holo Light theme
 */
public class LightProgressDialog extends ProgressDialog {

    /**
     * Create progress dialog
     *
     * @param context
     * @param resId
     * @return dialog
     */
    public static AlertDialog createAndShow(Context context, int resId) {
        return createAndShow(context, context.getResources().getString(resId));
    }

    /**
     * Create progress dialog
     *
     * @param context
     * @param message
     * @return dialog
     */
    public static AlertDialog createAndShow(Context context, CharSequence message) {
        AlertDialog dialog = null;
        if (SDK_INT > FROYO) {
            if (SDK_INT >= ICE_CREAM_SANDWICH)
                dialog = new LightProgressDialog(context, message);
            else {
                dialog = new ProgressDialog(context);
                dialog.setInverseBackgroundForced(true);
            }
            /*Typeface tf = Typeface.createFromAsset(context.getAssets(),"gillsansmt.TTF");
            TextView content = new TextView(context);
            content.setText(message);
            content.setTypeface(tf);
           dialog.setView(content);*/
            dialog.setMessage(message);
            
            ((ProgressDialog)dialog).setIndeterminate(true);
            ((ProgressDialog)dialog).setProgressStyle(STYLE_SPINNER);
            ((ProgressDialog)dialog).setIndeterminateDrawable(context.getResources().getDrawable(R.drawable.spinner));
        } else {
            dialog = LightAlertDialog.create(context);
            dialog.setInverseBackgroundForced(true);
            View view = LayoutInflater.from(context).inflate(
                    R.layout.progress_dialog, null);
            TextView tw = ((TextView) view.findViewById(R.id.tv_loading));
            		tw.setText(message);
   		 Typeface tf = Typeface.createFromAsset(context.getAssets(),"gillsansmt.TTF");
            tw.setTypeface(tf);
            dialog.setView(view);
        }
        dialog.setCancelable(false);
        /*Window window = dialog.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/
        dialog.show();
        return dialog;
    }

    private LightProgressDialog(Context context, CharSequence message) {
        super(context, THEME_HOLO_LIGHT);
    }
}
