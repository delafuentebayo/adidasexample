package adidas.interaction.adidassamplesdk;

import java.util.ArrayList;

import adidas.interaction.adidassamplesdk.adapter.TeamCustomAdapter;
import adidas.interaction.adidassamplesdk.utils.LightProgressDialog;
import adidas.sdk.worldcup.classes.Team;
import adidas.sdk.worldcup.utils.Requests;
import adidas.sdk.worldcup.utils.SdkResponse;
import android.app.Activity;
import android.app.AlertDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

/**
 * Fragment responsible to load and show the data of the teams. It has a custom
 * adapter to create a custom view with a list of objects Teams. This list is
 * shown in a listView It also implements SdkResponse, which will notify to the
 * fragment when the data is already loaded. This interface makes mandatory to
 * implement the method processFinish, which will update the adapter for the
 * listView
 * 
 * @author jesusmiguel
 * 
 */
public class TeamFragment extends Fragment implements SdkResponse {
	// Declaration of the necessary items

	ListView listViewTeam;
	ArrayList<Team> teams;
	TeamCustomAdapter adapter;
	Requests requests;
	private AlertDialog dialog;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */

	public static TeamFragment newInstance() {
		TeamFragment fragment = new TeamFragment();

		return fragment;
	}

	public TeamFragment() {
	}

	/**
	 * Creates the view
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_main, container,
				false);
		// We call to the sdk, in order to get the matches from internet
		requests = new Requests(Globals.TEAMS);
		// Assigning the callback

		requests.callback = this;
		paintData(rootView);
		teams = new ArrayList<Team>();
		adapter = new TeamCustomAdapter(getActivity(), teams);
		listViewTeam.setAdapter(adapter);
		// We create a loading dialog

		dialog = LightProgressDialog.createAndShow(getActivity(), getActivity()
				.getString(R.string.loading));
		
		return rootView;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);

	}

	/**
	 * Method to define the views
	 * 
	 * @param v
	 */
	public void paintData(View v) {
		listViewTeam = (ListView) v.findViewById(R.id.listView_team);

	}

	/**
	 * Method from the interface which will be called when the data downloaded
	 * is ready. In case there is no downloaded data, it shows a message.
	 */
	@Override
	public void processFinish() {
		// The fragment request the data

		teams = requests.getTeams();
		// In case no data, it shows an error

		if (teams.size() <= 0) {
			Toast.makeText(getActivity(),
					getActivity().getString(R.string.reason_no_load),
					Toast.LENGTH_SHORT).show();

		}
		// The dialog will be dismissed

		if (dialog != null)
			dialog.dismiss();
		// Refresh de listview with the new data

		adapter = new TeamCustomAdapter(getActivity(), teams);
		listViewTeam.setAdapter(adapter);
	}
}
