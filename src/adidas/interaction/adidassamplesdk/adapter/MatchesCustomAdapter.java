package adidas.interaction.adidassamplesdk.adapter;

import java.util.ArrayList;

import adidas.interaction.adidassamplesdk.R;
import adidas.sdk.worldcup.classes.Match;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Custom Adapter to show Matches
 * with the teams and the result
 * @author jesusmiguel
 *
 */

public class MatchesCustomAdapter extends BaseAdapter {
	private Context context;
	ArrayList<Match> matches;

	/**
	 * Constructor with parameters
	 * @param context
	 * @param matches
	 */
	public MatchesCustomAdapter(Context context, ArrayList<Match> matches) {
		this.context = context;
		this.matches = matches;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		View listView = convertView;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			listView = inflater.inflate(R.layout.custom_matches_listview, null);
		}
		//We declare the elements of the Adapter. In this case
		//they will be the teams and the results and we set
		//the data from the list of matches
		TextView match_number = (TextView) listView
				.findViewById(R.id.match_number);
		match_number.setText(context.getString(R.string.match_number).concat(
				String.valueOf(matches.get(position).getMatch_number())));
		TextView home_team = (TextView) listView.findViewById(R.id.home_team);
		home_team.setText(matches.get(position).getHome_team().getFifa_code());
		TextView home_result = (TextView) listView.findViewById(R.id.home_result);
		home_result.setText(String.valueOf(matches.get(position).getHome_goals()));
		
		TextView away_result = (TextView) listView.findViewById(R.id.away_result);
		away_result.setText(String.valueOf(matches.get(position).getAway_goals()));
		TextView away_team = (TextView) listView.findViewById(R.id.away_team);
		away_team.setText(matches.get(position).getAway_team().getFifa_code());
		

		return listView;
	}

	@Override
	public int getCount() {
		return matches.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}