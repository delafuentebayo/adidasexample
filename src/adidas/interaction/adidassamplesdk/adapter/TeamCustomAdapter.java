package adidas.interaction.adidassamplesdk.adapter;

import java.util.ArrayList;

import adidas.interaction.adidassamplesdk.R;
import adidas.sdk.worldcup.classes.Team;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Custom Adapter to show Teams with the nationality, code and group
 * 
 * @author jesusmiguel
 * 
 */

public class TeamCustomAdapter extends BaseAdapter {
	private Context context;
	ArrayList<Team> teams;

	public TeamCustomAdapter(Context context, ArrayList<Team> teams) {
		this.context = context;
		this.teams = teams;
	}

	public View getView(int position, View convertView, ViewGroup parent) {

		View listView = convertView;

		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			listView = inflater.inflate(R.layout.custom_team_listview, null);
		}

		// We declare the elements of the Adapter. In this case
		// they will be the nationality, the fifa_code
		// and the group
		TextView nationality = (TextView) listView
				.findViewById(R.id.nationality);
		nationality.setText(teams.get(position).getCountry());
		TextView code_fifa = (TextView) listView.findViewById(R.id.code);
		code_fifa.setText(teams.get(position).getFifa_code());
		TextView group = (TextView) listView.findViewById(R.id.group);
		group.setText(teams.get(position).getGroup_id());


		return listView;
	}

	@Override
	public int getCount() {
		return teams.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

}