package adidas.interaction.adidassamplesdk;

/**
 * Class to declare the globals responsible to create a connection to matches or
 * to teams
 * 
 * @author jesusmiguel
 * 
 */
public class Globals {
	public static final String MATCHES = "matches";
	public static final String TEAMS = "teams";

}
